﻿namespace Desk
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.labelNome = new System.Windows.Forms.Label();
            this.btnFazerCadastro = new System.Windows.Forms.Button();
            this.btnEnviarImagem = new System.Windows.Forms.Button();
            this.btnAlterarCadastro = new System.Windows.Forms.Button();
            this.btnExcluirCadastro = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnBuscarEmail = new System.Windows.Forms.Button();
            this.AlturaImag = new System.Windows.Forms.Label();
            this.txtAlturaImg = new System.Windows.Forms.TextBox();
            this.LarguraImg = new System.Windows.Forms.Label();
            this.txtLarguraImg = new System.Windows.Forms.TextBox();
            this.ImgInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(89, 48);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(278, 22);
            this.txtEmail.TabIndex = 0;
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(26, 53);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(42, 17);
            this.labelEmail.TabIndex = 1;
            this.labelEmail.Text = "Email";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(89, 110);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(278, 22);
            this.txtNome.TabIndex = 2;
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(26, 115);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(45, 17);
            this.labelNome.TabIndex = 3;
            this.labelNome.Text = "Nome";
            this.labelNome.Click += new System.EventHandler(this.Label2_Click);
            // 
            // btnFazerCadastro
            // 
            this.btnFazerCadastro.Location = new System.Drawing.Point(40, 243);
            this.btnFazerCadastro.Name = "btnFazerCadastro";
            this.btnFazerCadastro.Size = new System.Drawing.Size(139, 27);
            this.btnFazerCadastro.TabIndex = 7;
            this.btnFazerCadastro.Text = "Fazer Cadastro";
            this.btnFazerCadastro.UseVisualStyleBackColor = true;
            this.btnFazerCadastro.Click += new System.EventHandler(this.BtnFazerCadastro_Click);
            // 
            // btnEnviarImagem
            // 
            this.btnEnviarImagem.Location = new System.Drawing.Point(40, 305);
            this.btnEnviarImagem.Name = "btnEnviarImagem";
            this.btnEnviarImagem.Size = new System.Drawing.Size(139, 27);
            this.btnEnviarImagem.TabIndex = 8;
            this.btnEnviarImagem.Text = "Enviar Sinal";
            this.btnEnviarImagem.UseVisualStyleBackColor = true;
            this.btnEnviarImagem.Click += new System.EventHandler(this.BtnEnviarImagem_Click);
            // 
            // btnAlterarCadastro
            // 
            this.btnAlterarCadastro.Location = new System.Drawing.Point(211, 243);
            this.btnAlterarCadastro.Name = "btnAlterarCadastro";
            this.btnAlterarCadastro.Size = new System.Drawing.Size(139, 27);
            this.btnAlterarCadastro.TabIndex = 9;
            this.btnAlterarCadastro.Text = "Alterar Cadastro";
            this.btnAlterarCadastro.UseVisualStyleBackColor = true;
            this.btnAlterarCadastro.Click += new System.EventHandler(this.BtnAlterarCadastro_Click);
            // 
            // btnExcluirCadastro
            // 
            this.btnExcluirCadastro.Location = new System.Drawing.Point(387, 243);
            this.btnExcluirCadastro.Name = "btnExcluirCadastro";
            this.btnExcluirCadastro.Size = new System.Drawing.Size(139, 27);
            this.btnExcluirCadastro.TabIndex = 10;
            this.btnExcluirCadastro.Text = "Excluir Cadastro";
            this.btnExcluirCadastro.UseVisualStyleBackColor = true;
            this.btnExcluirCadastro.Click += new System.EventHandler(this.BtnExcluirCadastro_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.Location = new System.Drawing.Point(211, 305);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(139, 27);
            this.btnFechar.TabIndex = 11;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.BtnFechar_Click);
            // 
            // btnBuscarEmail
            // 
            this.btnBuscarEmail.Location = new System.Drawing.Point(387, 43);
            this.btnBuscarEmail.Name = "btnBuscarEmail";
            this.btnBuscarEmail.Size = new System.Drawing.Size(139, 27);
            this.btnBuscarEmail.TabIndex = 12;
            this.btnBuscarEmail.Text = "Pesquisar";
            this.btnBuscarEmail.UseVisualStyleBackColor = true;
            this.btnBuscarEmail.Click += new System.EventHandler(this.BtnBuscarEmail_Click);
            // 
            // AlturaImag
            // 
            this.AlturaImag.AutoSize = true;
            this.AlturaImag.Location = new System.Drawing.Point(37, 189);
            this.AlturaImag.Name = "AlturaImag";
            this.AlturaImag.Size = new System.Drawing.Size(49, 17);
            this.AlturaImag.TabIndex = 13;
            this.AlturaImag.Text = "Altura:";
            // 
            // txtAlturaImg
            // 
            this.txtAlturaImg.Location = new System.Drawing.Point(92, 186);
            this.txtAlturaImg.Name = "txtAlturaImg";
            this.txtAlturaImg.Size = new System.Drawing.Size(44, 22);
            this.txtAlturaImg.TabIndex = 14;
            // 
            // LarguraImg
            // 
            this.LarguraImg.AutoSize = true;
            this.LarguraImg.Location = new System.Drawing.Point(178, 189);
            this.LarguraImg.Name = "LarguraImg";
            this.LarguraImg.Size = new System.Drawing.Size(62, 17);
            this.LarguraImg.TabIndex = 15;
            this.LarguraImg.Text = "Largura:";
            // 
            // txtLarguraImg
            // 
            this.txtLarguraImg.Location = new System.Drawing.Point(246, 186);
            this.txtLarguraImg.Name = "txtLarguraImg";
            this.txtLarguraImg.Size = new System.Drawing.Size(44, 22);
            this.txtLarguraImg.TabIndex = 16;
            // 
            // ImgInfo
            // 
            this.ImgInfo.AutoSize = true;
            this.ImgInfo.Location = new System.Drawing.Point(40, 156);
            this.ImgInfo.Name = "ImgInfo";
            this.ImgInfo.Size = new System.Drawing.Size(162, 17);
            this.ImgInfo.TabIndex = 17;
            this.ImgInfo.Text = "Informações da Imagem:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ImgInfo);
            this.Controls.Add(this.txtLarguraImg);
            this.Controls.Add(this.LarguraImg);
            this.Controls.Add(this.txtAlturaImg);
            this.Controls.Add(this.AlturaImag);
            this.Controls.Add(this.btnBuscarEmail);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnExcluirCadastro);
            this.Controls.Add(this.btnAlterarCadastro);
            this.Controls.Add(this.btnEnviarImagem);
            this.Controls.Add(this.btnFazerCadastro);
            this.Controls.Add(this.labelNome);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.txtEmail);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label labelNome;
        private System.Windows.Forms.Button btnFazerCadastro;
        private System.Windows.Forms.Button btnEnviarImagem;
        private System.Windows.Forms.Button btnAlterarCadastro;
        private System.Windows.Forms.Button btnExcluirCadastro;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnBuscarEmail;
        private System.Windows.Forms.Label AlturaImag;
        private System.Windows.Forms.TextBox txtAlturaImg;
        private System.Windows.Forms.Label LarguraImg;
        private System.Windows.Forms.TextBox txtLarguraImg;
        private System.Windows.Forms.Label ImgInfo;
    }
}

