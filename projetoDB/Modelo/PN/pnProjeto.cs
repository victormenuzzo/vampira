﻿using Modelo.DAO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.PN
{
    public class pnProjeto
    {
        public static bool Inserir(Usuario p)
        {
            try
            {
                projetoBDEntities db = new projetoBDEntities();

                db.Usuarios.Add(p);
                db.SaveChanges();
                return true;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public static bool Alterar(Usuario p)
        {
            try
            {
                projetoBDEntities db = new projetoBDEntities();
                Usuario pa = new Usuario();

                pa = db.Usuarios.Find(p.Email);

                pa.Nome = p.Nome;

                db.SaveChanges();

                return true;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public static bool Excluir(Usuario p)
        {
            try
            {
                projetoBDEntities db = new projetoBDEntities();
                Usuario pa = new Usuario();

                pa = db.Usuarios.Find(p.Email);

                db.Usuarios.Remove(pa);

                db.SaveChanges();

                return true;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public static Usuario Pesquisar(string email)
        {
            try
            {
                projetoBDEntities db = new projetoBDEntities();
                Usuario pa = new Usuario();

                pa = db.Usuarios.Find(email);

                return pa;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public static bool Inserir_Imagem(Imagem image)
        {
            try
            {
                projetoBDEntities db = new projetoBDEntities();

                db.Imagems.Add(image);
                db.SaveChanges();
                return true;
            }

            catch (Exception)
            {
                throw;
            }
        }
    }

}
