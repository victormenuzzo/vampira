﻿CREATE TABLE [dbo].[Reconstrucao]
(
	[Id_Reconstrucao] INT NOT NULL PRIMARY KEY, 
    [Inicio] DATETIME NOT NULL, 
    [Fim] DATETIME NOT NULL, 
    [Numero_Iteracoes] INT NOT NULL
	CONSTRAINT [Reconstrucao_Imagem] FOREIGN KEY ([Id_Imagem]) REFERENCES [Imagem]([Id_Imagem]), 
    [Id_Imagem] INT NOT NULL

)
